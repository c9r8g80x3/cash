import os
import copy
import logging
from django.utils.log import DEFAULT_LOGGING

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = '9n7e53dm2bmvv-f9i7yq!^p-854epbc*poek=kju0*)m58h23m'
PROJECT_ROOT = os.path.dirname(__file__)

DEBUG = True

ALLOWED_HOSTS = ['*']
INTERNAL_IPS = ('127.0.0.1',)

LOGGING = copy.deepcopy(DEFAULT_LOGGING)
LOGGING['filters']['suppress_deprecated'] = {
    '()': 'settings.base.SuppressDeprecated'
}
LOGGING['handlers']['console']['filters'].append('suppress_deprecated')


class SuppressDeprecated(logging.Filter):
    def filter(self, record):
        WARNINGS_TO_SUPPRESS = [
            'RemovedInDjango18Warning',
            'RemovedInDjango19Warning'
        ]
        return not any(
            [warn in record.getMessage() for warn in WARNINGS_TO_SUPPRESS])


INSTALLED_APPS = (
    'django.contrib.contenttypes',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',

    'logentry_admin',
    'debug_toolbar',
    'annoying',

    'apps.default',
)

MIDDLEWARE_CLASSES = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',

    'debug_toolbar.middleware.DebugToolbarMiddleware',  # Debug toolbar
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# App
SITE_URL = 'http://localhost.com:8000'

LANGUAGE_CODE = 'ru-RU'
TIME_ZONE = 'Asia/Tashkent'
USE_I18N = True
USE_L10N = True
USE_TZ = False
ROOT_URLCONF = 'urls'
USE_ETAGS = True
WSGI_APPLICATION = 'wsgi.application'
APPEND_SLASH = True
SITE_ID = 1

STATIC_URL = '/static/'
# STATIC_ROOT = BASE_DIR + '/static/'
STATICFILES_DIRS = (
    os.path.join(PROJECT_ROOT, '../', 'static/'),
)

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(PROJECT_ROOT, '..', 'media/')

# Mail Config
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025
EMAIL_USE_TLS = False
EMAIL_USE_SSL = False

try:
    from settings.local import *
except ImportError:
    from settings.production import *
