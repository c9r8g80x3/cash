# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import apps.default.models
from django.conf import settings
import datetime


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('default', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FileStorage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('file', models.FileField(upload_to=apps.default.models.file_directory_path)),
                ('created_at', models.DateTimeField(default=datetime.datetime.now, verbose_name='Время импорта')),
                ('user', models.ForeignKey(related_name='Пользователь', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Файл',
                'verbose_name_plural': 'Файлы',
                'ordering': ['-id'],
            },
        ),
        migrations.AddField(
            model_name='cashlog',
            name='file',
            field=models.ForeignKey(default=None, related_name='Файл', to='default.FileStorage'),
        ),
    ]
