# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('default', '0002_auto_20170222_0032'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cashlog',
            name='created_at',
            field=models.DateField(verbose_name='Дата, время.'),
        ),
    ]
