# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('default', '0003_auto_20170222_1408'),
    ]

    operations = [
        migrations.AddField(
            model_name='filestorage',
            name='ip',
            field=models.GenericIPAddressField(default=None, verbose_name='IP-адрес', null=True),
        ),
    ]
