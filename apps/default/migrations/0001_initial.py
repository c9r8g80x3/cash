# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CashLog',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('unique_id', models.CharField(default=False, null=True, max_length=255, db_index=True)),
                ('inn', models.BigIntegerField(verbose_name='ИНН')),
                ('sum', models.FloatField(verbose_name='Сумма')),
                ('created_at', models.DateTimeField(verbose_name='Дата, время.')),
            ],
            options={
                'verbose_name_plural': 'Взносы',
                'ordering': ['-id'],
                'verbose_name': 'Взнос',
            },
        ),
    ]
