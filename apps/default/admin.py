from django.contrib import admin
from apps.default.models import CashLog, FileStorage


class CashLogAdmin(admin.ModelAdmin):
    list_display = ('file', 'inn', 'sum', 'created_at')
    list_filter = ('inn', )

admin.site.register(FileStorage)
admin.site.register(CashLog, CashLogAdmin)

