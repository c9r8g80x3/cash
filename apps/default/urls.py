from django.conf.urls import url
from apps.default import views

urlpatterns = [
    url(r'^$', views.auth_view, name='main.auth'),
    url(r'^index/$', views.index_view, name='main.index'),
]
