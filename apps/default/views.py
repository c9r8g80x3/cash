from uuid import uuid1

from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from handy.decorators import render_to
from apps.default.models import FileStorage, CashLog
from datetime import datetime


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')

    return ip


@render_to('auth.html')
def auth_view(request):
    if request.method == 'POST':
        user = authenticate(
            username=request.POST.get('login', None),
            password=request.POST.get('password', None))

        if user is not None:
            login(request, user)

            return redirect(reverse('main.index'))
        else:
            messages.error(request, 'Неправильный логин или пароль.')

    return {}


@login_required(login_url='main.auth')
@render_to('index.html')
def index_view(request):
    data_to_db, errors, message = [], [], []
    if request.method == 'POST':
        file = request.FILES.get('file')
        if file.name.endswith('.csv'):
            file_data = file.read().decode('utf-8')

            db_instance = FileStorage.objects.create(
                user=request.user,
                ip=get_client_ip(request),
                file=file)
            csv_row = file_data.split('\n')
            for idx, row in enumerate(csv_row):
                try:
                    col = row.split(',')
                    inn = col[0]
                    cash = col[1]
                    date = col[2]

                    if inn.isdigit() and cash.isdigit():
                        date = datetime.strptime(date, '%d.%m.%Y').date()

                        data_to_db.append(CashLog(
                            unique_id=uuid1(),
                            file=db_instance,
                            created_at=date,
                            inn=inn, sum=cash
                        ))
                    else:
                        raise Exception
                except Exception as e:
                    print(e)
                    errors.append({
                        'message': 'Ошибка в строке: #{0}'.format(idx+1)
                    })

            if len(errors) == 0:
                CashLog.objects.bulk_create(data_to_db)
                message.append({
                    'message': 'Данные успешно занесены.'
                })
        else:
            errors.append({
                'message': 'Ошибка в формате файла'
            })
    data_list = CashLog.objects.filter(file__user=request.user)
    paginator = Paginator(data_list, 25)

    page = request.GET.get('page')
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)

    return {
        'data': data,
        'message': message,
        'errors': errors
    }
