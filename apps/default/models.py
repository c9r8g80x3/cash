from uuid import uuid1
from datetime import datetime
from django.contrib.auth.models import User
from django.db import models


def file_directory_path(instance, filename):
    return 'uploads/{0}-{1}-{2}/{3}.{4}'.format(
        datetime.now().day, datetime.now().month, datetime.now().year,
        uuid1(), filename.split('.')[-1])


class FileStorage(models.Model):
    user = models.ForeignKey(User, related_name='Пользователь')
    file = models.FileField(upload_to=file_directory_path)
    ip = models.GenericIPAddressField(default=None, null=True, verbose_name='IP-адрес')
    created_at = models.DateTimeField(default=datetime.now, verbose_name='Время импорта')

    class Meta:
        verbose_name = 'Файл'
        verbose_name_plural = 'Файлы'
        ordering = ['-id']

    def __str__(self):
        return '{0} - {1}'.format(self.user, datetime.strftime(self.created_at, '%d-%m-%Y'))


class CashLog(models.Model):
    unique_id = models.CharField(max_length=255, default=False, null=True, db_index=True)
    file = models.ForeignKey(FileStorage, related_name='Файл', null=False, default=None)
    inn = models.BigIntegerField(verbose_name='ИНН')
    sum = models.FloatField(verbose_name='Сумма')
    created_at = models.DateField(verbose_name='Дата, время.')

    class Meta:
        verbose_name = 'Взнос'
        verbose_name_plural = 'Взносы'
        ordering = ['-id']
